export const version = "2.0.0-beta";

export class Stage {
	constructor(config) {
		this.scene = null;
		
		this.renderer = null;
		
		this.width = config.width || 0;
		this.height = config.height || 0;
		
		this.multiplierX = config.multiplierX || 1;
		this.multiplierY = config.multiplierY || 1;
		
		const isAntialiased = config.isAntialiased || false;
		
		this.element = document.createElement("div");
		this.canvasElement = document.createElement("canvas");
		this.canvasContext = this.canvasElement.getContext("2d");
		this.canvasStyle = window.getComputedStyle(this.canvasElement);
		
		this.element.className = "gamestaddle-stage";
		
		this.canvasElement.width = this.width * this.multiplierX;
		this.canvasElement.height = this.height * this.multiplierY;
		
		this.canvasContext.imageSmoothingEnabled = isAntialiased;
		this.canvasContext.textBaseline = "top";
		
		const virtualXOf = (realX) =>
			realX /
			+this.canvasStyle.width.slice(0, -2) *
			this.width;
		const virtualYOf = (realY) =>
			realY /
			+this.canvasStyle.height.slice(0, -2) *
			this.height;
		
		this.canvasElement.addEventListener("mousedown", (event) => {
			if (this.scene) {
				this.scene.onMousePress(
					virtualXOf(event.offsetX),
					virtualYOf(event.offsetY)
				);
			}
			
			event.preventDefault();
		});
		this.canvasElement.addEventListener("touchstart", (event) => {
			if (this.scene) {
				const canvasRect = this.canvasElement.getBoundingClientRect();
				const canvasX = canvasRect.left;
				const canvasY = canvasRect.top;
				
				this.scene.onMouseMove(
					virtualXOf(event.changedTouches[0].clientX - canvasX),
					virtualYOf(event.changedTouches[0].clientY - canvasY)
				);
				this.scene.onMousePress(
					virtualXOf(event.changedTouches[0].clientX - canvasX),
					virtualYOf(event.changedTouches[0].clientY - canvasY)
				);
			}
			
			event.preventDefault();
		});
		this.canvasElement.addEventListener("mousemove", (event) => {
			if (this.scene) {
				this.scene.onMouseMove(
					virtualXOf(event.offsetX),
					virtualYOf(event.offsetY)
				);
			}
			
			event.preventDefault();
		});
		this.canvasElement.addEventListener("touchmove", (event) => {
			if (this.scene) {
				const canvasRect = this.canvasElement.getBoundingClientRect();
				const canvasX = canvasRect.left;
				const canvasY = canvasRect.top;
				
				this.scene.onMouseMove(
					virtualXOf(event.changedTouches[0].clientX - canvasX),
					virtualYOf(event.changedTouches[0].clientY - canvasY)
				);
			}
			
			event.preventDefault();
		});
		this.canvasElement.addEventListener("mouseup", (event) => {
			if (this.scene) {
				this.scene.onMouseRelease(
					virtualXOf(event.offsetX),
					virtualYOf(event.offsetY)
				);
			}
			
			event.preventDefault();
		});
		this.canvasElement.addEventListener("touchend", (event) => {
			if (this.scene) {
				const canvasRect = this.canvasElement.getBoundingClientRect();
				const canvasX = canvasRect.left;
				const canvasY = canvasRect.top;
				
				this.scene.onMouseRelease(
					virtualXOf(event.changedTouches[0].clientX - canvasX),
					virtualYOf(event.changedTouches[0].clientY - canvasY)
				);
			}
			
			event.preventDefault();
		});
		
		this.element.appendChild(this.canvasElement);
	}
	
	setupErrorHandler() {
		window.addEventListener("error", (event) => {
			try {
				this.die();
			} catch(_) {}
			
			if (event.error instanceof Error) {
				alert(
`Game crashed

${event.error.name}: ${event.error.message}
${event.error.stack}`
				);
			} else {
				alert(
`
Game crashed

But, for some reason, the error could not be caught; check your browser's devtools for more info
`
				);
			}
		});
	}
	
	setupKeyboard(container) {
		container.addEventListener("keydown", (event) => {
			if (this.scene) {
				this.scene.onKeyPress(event.code);
			}
			
			event.preventDefault();
		});
		container.addEventListener("keyup", (event) => {
			if (this.scene) {
				this.scene.onKeyRelease(event.code);
			}
			
			event.preventDefault();
		});
	}
	
	fillEverything() {
		document.body.style.overflow = "hidden";
		
		this.element.style.position = "fixed";
		this.element.style.top = "0";
		this.element.style.left = "0";
		this.element.style.display = "flex";
		this.element.style.justifyContent = "center"
		this.element.style.alignItems = "center"
		this.element.style.width = "100vw";
		this.element.style.height = "100vh";
		
		this.canvasElement.style.flexShrink = "0";
		this.canvasElement.style.border = "none";
		
		window.addEventListener("resize", () => {
			const windowAspectRatio = window.innerWidth / window.innerHeight;
			const gameAspectRatio = this.canvasElement.width / this.canvasElement.height;
			
			if (windowAspectRatio > gameAspectRatio) {
				this.canvasElement.style.width = 100 * gameAspectRatio + "vh";
				this.canvasElement.style.height = "100vh";
			} else if (windowAspectRatio < gameAspectRatio) {
				this.canvasElement.style.width = "100vw";
				this.canvasElement.style.height = 100 * (
					1 / gameAspectRatio
				) + "vw";
			} else {
				this.canvasElement.style.width = "100vw";
				this.canvasElement.style.height = "100vh";
			}
		});
		
		window.dispatchEvent(new Event("resize"));
	}
	
	enslaveWindow() {
		this.setupErrorHandler();
		
		this.setupKeyboard(document.body);
		
		this.fillEverything();
		
		document.body.appendChild(this.element);
	}
	
	render() {
		this.renderer = requestAnimationFrame(() => {
			this.render();
		});
		
		if (!this.scene) {
			throw new ReferenceError("scene is not set");
		}
		
		this.canvasContext.clearRect(
			0,
			0,
			
			this.canvasElement.width,
			this.canvasElement.height
		);
		
		this.scene.onPreupdate();
		this.scene.onUpdate();
		
		this.scene.onRender(this);
	}
	
	die() {
		this.scene = null;
		
		cancelAnimationFrame(this.renderer);
	}
}

export class Scene {
	constructor(contents) {
		this.layers = [];
		
		if (contents) {
			Object.assign(this, contents);
		}
	}
	
	onRender(stage) {
		for (const layer of this.layers) {
			for (const actor of layer) {
				actor.onRender(stage);
			}
		}
	}
	
	onPreupdate() {
		for (const layer of this.layers) {
			for (const actor of layer) {
				actor.onUpdate();
			}
		}
	}
	onUpdate() {}
	
	onKeyPress(code) {}
	onKeyRelease(code) {}
	
	onMousePress(x, y) {
		for (let i = this.layers.length - 1; i >= 0; i--) {
			for (let j = this.layers[i].length - 1; j >= 0; j--) {
				if (this.layers[i][j].canBeHitAt(x, y)) {
					this.layers[i][j].onMousePress(x, y);
					
					return;
				}
			}
		}
	}
	onMouseMove(x, y) {
		for (let i = this.layers.length - 1; i >= 0; i--) {
			for (let j = this.layers[i].length - 1; j >= 0; j--) {
				if (this.layers[i][j].canBeHitAt(x, y)) {
					this.layers[i][j].onMouseMove(x, y);
					
					return;
				}
			}
		}
	}
	onMouseRelease(x, y) {
		for (let i = this.layers.length - 1; i >= 0; i--) {
			for (let j = this.layers[i].length - 1; j >= 0; j--) {
				if (this.layers[i][j].canBeHitAt(x, y)) {
					this.layers[i][j].onMouseRelease(x, y);
					
					return;
				}
			}
		}
	}
	
	checkCollisions(...layers) {
		for (const layer of layers) {
			for (const layer1 of layers) {
				for (const actor of layer) {
					for (const actor1 of layer1) {
						if (actor !== actor1 && actor.collidesWith(actor1)) {
							actor.onCollision(actor1);
						}
					}
				}
			}
		}
	}
}

export class Actor {
	constructor(contents) {
		this.renderables = [];
		
		if (contents) {
			Object.assign(this, contents);
		}
	}
	
	onUpdate() {}
	
	onRender(stage) {
		for (const renderable of this.renderables) {
			renderable.render(stage);
		}
	}
	
	onMousePress(x, y) {}
	onMouseMove(x, y) {}
	onMouseRelease(x, y) {}
	
	onCollision(actor) {}
	
	collidesWith(actor) {
		for (const renderable of this.renderables) {
			for (const renderable1 of actor.renderables) {
				if (renderable.collidesWith(renderable1)) {
					return true;
				}
			}
		}
		return false;
	}
	
	canBeHitAt(x, y) {
		for (const renderable of this.renderables) {
			if (renderable.canBeHitAt(x, y)) {
				return true;
			}
		}
		return false;
	}
}

export class Renderable {
	constructor(config) {
		config = config || {};
		
		if ("opacity" in config) {
			this.opacity = config.opacity;
		}
		
		if ("rotation" in config) {
			this.rotation = config.rotation;
		}
		
		if ("scaleX" in config) {
			this.scaleX = config.scaleX;
		}
		if ("scaleY" in config) {
			this.scaleY = config.scaleY;
		}
	}
	
	prerender(stage) {
		if ("opacity" in this) {
			stage.canvasContext.globalAlpha = this.opacity;
		}
		
		if (
			"rotation" in this ||
			
			"scaleX" in this ||
			"scaleY" in this
		) {
			const hitbox = this.getHitbox();
			
			stage.canvasContext.translate(
				(hitbox.x + hitbox.width / 2) * stage.multiplierX,
				(hitbox.y + hitbox.height / 2) * stage.multiplierY
			);
			
			if ("rotation" in this) {
				stage.canvasContext.rotate(this.rotation * Math.PI / 180);
			}
			
			if ("scaleX" in this || "scaleY" in this) {
				stage.canvasContext.scale(
					this.scaleX || 1,
					this.scaleY || 1
				);
			}
			
			stage.canvasContext.translate(
				-(hitbox.x + hitbox.width / 2) * stage.multiplierX,
				-(hitbox.y + hitbox.height / 2) * stage.multiplierY
			);
		}
	}
	render(stage) {}
	postrender(stage) {
		stage.canvasContext.globalAlpha = 1;
		
		stage.canvasContext.resetTransform();
	}
	
	getHitbox() {
		return {
			x: this.x,
			y: this.y,
			
			width: this.width,
			height: this.height
		};
	}
	
	canBeHitAt(x, y) {
		const hitbox = this.getHitbox();
		
		if (
			x >= hitbox.x &&
			x < hitbox.x + hitbox.width &&
			y >= hitbox.y &&
			y < hitbox.y + hitbox.height
		) {
			return true;
		}
		return false;
	}
	
	collidesWith(renderable) {
		const hitbox = this.getHitbox();
		const hitbox1 = renderable.getHitbox();
		
		if (
			hitbox.x < hitbox1.x + hitbox1.width &&
			hitbox.x + hitbox.width > hitbox1.x &&
			hitbox.y < hitbox1.y + hitbox1.height &&
			hitbox.y + hitbox.height > hitbox1.y
		) {
			return true;
		}
		return false;
	}
}

export class Sprite extends Renderable {
	constructor(config) {
		super(config);
		
		config = config || {};
		
		this.texture = config.texture;
		
		this.x0 = config.x0 || 0;
		this.y0 = config.y0 || 0;
		
		this.width0 = config.width0 || this.width0;
		this.height0 = config.height0 || this.height0;
		
		this.x = config.x || 0;
		this.y = config.y || 0;
		
		this.width = config.width || this.texture.width;
		this.height = config.height || this.texture.height;
	}
	
	get texture() {
		return this._texture;
	}
	set texture(value) {
		if (!value) {
			throw new ReferenceError("missing texture");
		}
		
		this._texture = value;
		
		this.width0 = value.width;
		this.height0 = value.height;
		
		this.width = value.width;
		this.height = value.height;
	}
	
	render(stage) {
		this.prerender(stage);
		
		stage.canvasContext.drawImage(
			this.texture,
			
			this.x0,
			this.y0,
			
			this.width0,
			this.height0,
			
			this.x * stage.multiplierX,
			this.y * stage.multiplierY,
			
			this.width * stage.multiplierX,
			this.height * stage.multiplierY
		);
		
		this.postrender(stage);
	}
}

export class Text extends Renderable {
	constructor(config) {
		super(config);
		
		config = config || {};
		
		this.string = config.string || "Sample Text";
		
		this.align = config.align || "left";
		
		this.font = config.font || "sans-serif";
		this.fontSize = config.fontSize || "12";
		
		this.color = config.color || "#000000";
		
		this.x = config.x || 0;
		this.y = config.y || 0;
	}
	
	get string() {
		return this._lines.join("\n");
	}
	set string(value) {
		this._lines = value.split("\n");
	}
	
	get width() {
		return this._width;
	}
	set width(_) {}
	
	get height() {
		return (
			this._lines.length *
			Math.floor(1.25 * this.fontSize) -
			Math.floor(0.25 * this.fontSize)
		);
	}
	set height(_) {}
	
	render(stage) {
		this.prerender(stage);
		
		let longestLine = "";
		
		const lineInterval = Math.floor(1.25 * this.fontSize);
		
		stage.canvasContext.textAlign = this.align;
		
		stage.canvasContext.font = `${this.fontSize * stage.multiplierX}px ${this.font}`;
		
		stage.canvasContext.fillStyle = this.color;
		
		this._lines.forEach((line, number) => {
			if (line.length > longestLine.length) {
				longestLine = line;
			}
			
			stage.canvasContext.fillText(
				line,
				
				this.x * stage.multiplierX,
				(this.y + number * lineInterval) * stage.multiplierY
			);
		});
		
		this._width = stage.canvasContext.measureText(
			longestLine
		).width / stage.multiplierX;
		
		this.postrender(stage);
	}
	
	getHitbox() {
		switch (this.align) {
		case "left":
			return {
				x: this.x,
				y: this.y,
				
				width: this.width,
				height: this.height
			};
		case "center":
			return {
				x: this.x - this.width / 2,
				y: this.y,
				
				width: this.width,
				height: this.height
			};
		case "right":
			return {
				x: this.x - this.width,
				y: this.y,
				
				width: this.width,
				height: this.height
			};
		default:
			throw new TypeError("invalid text align");
		}
	}
}

export class ConfigStorage extends Map {
	load(name, fallback) {
		return new Promise((resolve, reject) => {
			const value = window.localStorage.getItem(name);
			const realFallback = JSON.stringify(fallback);
			
			if (value !== null) {
				this.set(name, JSON.parse(value));
			} else {
				window.localStorage.setItem(name, realFallback);
				
				this.set(name, JSON.parse(realFallback));
			}
			
			resolve();
		});
	}
	
	write(name) {
		return new Promise((resolve, reject) => {
			window.localStorage.setItem(name,
				JSON.stringify(this.get(name))
			);
			
			resolve();
		});
	}
	
	clear() {
		return new Promise((resolve, reject) => {
			window.localStorage.clear();
			
			resolve();
		});
	}
}

export class TextureStorage extends Map {
	load(path) {
		return new Promise((resolve, reject) => {
			const image = new Image();
			
			image.src = path;
			
			image.addEventListener("load", () => {
				this.set(path, image);
				
				resolve(image);
			});
			image.addEventListener("error", () => {
				reject(new ReferenceError(`can't load texture ${path}`));
			});
		});
	}
}
export class SoundStorage extends Map {
	load(path) {
		return new Promise((resolve, reject) => {
			const sound = new Audio();
			
			sound.src = path;
			
			sound.addEventListener("canplay", () => {
				this.set(path, sound);
				
				resolve(sound);
			});
			sound.addEventListener("error", () => {
				reject(new ReferenceError(`can't load sound ${path}`));
			});
		});
	}
}
